# SOLUTIONS à l'exercice avec GitLab

**1. Créer un compte sur GitLab**

Avec l'interface web

**2. Créer un dépôt**

Avec l'interface web. Après la création du dépôt, des instructions s'afficheront sur comment ajouter du contenu à partir d'un dossier local etc.

**3. Le lier à un dossier local sur son ordinateur**

Par exemple, une des façons d'ajouter du contenu local serait avec les commandes suivantes dans un dossier local :

```
git init
git remote add origin https://gitlab.com/user-name/nom-du-depot.git
```

Mais c'est n'est pas la seule option : On pourrait aussi créer le dépôt sur l'interface, le cloner *vide* sur nos postes et commencer à ajouter des fichiers etc. à partir de ce dépôt vide cloné.


**4. Créer un fichier dans le dossier de travail**

    - Par exemple, cette sympathique vache issue de la commande `cowsay hi`

```
 ____
< hi >
 ----
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

Le fichier va s'appeler `fichier-cow.txt`

`touch fichier-cow.txt`

**5. Ajouter le fichier à l'index (`add`)**

`git add fichier-cow.txt`

**6. Valider l'ajout (`commit`)**

`git commit -m "ajout fichier vache 1" fichier-cow.txt`


**7. Modifier le fichier, valider le changement à nouveau**

    - Par exemple, on pourrait dessiner les yeux de la sympathique vache comme des "x", pas comme des "o"

```
 ____
< hi >
 ----
        \   ^__^
         \  (xx)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```
- Pour valider le changement, il y a plusieurs façons de le faire

1. avec `add` et `commit`

```
git add fichier-cow.txt
git commit -m "modif fichier vache" fichier-cow.txt
```

2. avec `commit -am` (qui va également valider tous les autres changements effectués, s'il y en a)

`git commit -am "modif fichier vache"`

3. En mentionnant le nom du fichier, il n'est pas obligatoire de faire `add` avant valider la modification : 

`git commit -m "modif fichier vache" fichier-cow.txt`


**8. Afficher la différence entre les deux révisions**

La révision courante est contenu dans l'id HEAD. La révision précédente est dans HEAD^. la commande pour afficher la différence entre la dernière et l'avant-dernière révision est donc la suivante:

`git diff HEAD^ HEAD`

Pour le diff entre l'avant dernière révision et celle avant ce serait:

`git diff HEAD^^ HEAD^`

On peut également comparer des révisions selon leur ID alphanumérique, c.à.d pour deux révisions abcde16 et 123456a, la commande serait : 

`git diff abcde16 123456a`

À noter que la commande `diff` affiche par défaut une comparaison à niveau des lignes, mais on peut afficher les différences à l'intérieur de chaque ligne avec `git diff --word-diff`

**9. Afficher l'historique des révisions**

`git log`

**10. Créer une branche.**

    - Elle pourrait s'appeler par exemple `multicow` et contenir un nouveau fichier avec des multiples dessins de la vache.

`git checkout -b multicow`

**11. Créer un nouveau fichier dans la branche (ou modifier un fichier existant)**

```
touch un_fichier.txt̀
git add  un_fichier.txt
git commit -am "ajout de fichier"
```

**12. Revenir vers la branche originale et fusionner les modifications de la branche créée**

```
git checkout master
git merge multicow
```

**13. Ajouter un tag annoté sur la nouvelle révision**

`git tag -a "v1-multicow" -m "révision créée après merger la branche multicow"`

**14. Ajouter des membres sur son projet GitLab**

On peut le faire avec l'interface : Depuis la page du projet, aller sur *Settings / Members*

**15. Cloner un dépôt publique de votre intérêt à partir de n'importe quelle plate-forme de dév (GitHub, Bitbucket, GitLab ...)**

`git clone https://url-du-depot.git`

**16. Comment faire pour cloner une *branche spécifique* d'un dépôt ? Par exemple, le projet [nematus](https://github.com/EdinburghNLP/nematus), en traduction neuronale, a une branche nommée *theano*. Comment cloner celle-ci, et pas les autres?**

Deux possibilités :

- `git clone https://github.com/EdinburghNLP/nematus.git --branch theano --single-branch nematus-theano-branch-only`, où `nematus-theano-branch-only` est le nom du dossier où la branche va être clonée
- `git clone --single-branch -b theano https://github.com/EdinburghNLP/nematus.git`. La branche sera clonée dans un dossier appelé `nematus`

**17. Que veut dire la commande `diff HEAD^ HEAD` ?**

Déjà vu dans la question 8. Ça compare l'avant dernière révision avec l'avant-dernière (autrement dit, la version précédente et la courante)
