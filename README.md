# Exercice avec FramaGit / GitLab

1. Créer un compte sur FramaGit (ou GitLab)
2. Créer un dépôt
3. Le lier à un dossier local sur son ordinateur
4. Créer un fichier dans le dossier de travail

    - Par exemple, cette sympathique vache issue de la commande `cowsay hi`

```
 ____
< hi >
 ----
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```


5. Ajouter le fichier à l'index (`add`)
6. Valider l'ajout (`commit`)
7. Modifier le fichier, valider le changement à nouveau

    - Par exemple, on pourrait dessiner les yeux de la sympathique vache comme des "x", pas comme des "o"

```
 ____
< hi >
 ----
        \   ^__^
         \  (xx)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```


8. Afficher la différence entre les deux révisions

9. Afficher l'historique des révisions

10. Créer une branche.

    - Elle pourrait s'appeler par exemple `multicow` et contenir un nouveau fichier avec des multiples dessins de la vache.

11. Créer un nouveau fichier dans la branche (ou modifier un fichier existant)
12. Revenir vers la branche originale et fusionner les modifications de la branche créée

13. Ajouter un tag annoté sur la nouvelle révision

14. Ajouter des membres sur son projet GitLab

15. Cloner un dépôt publique de votre intérêt à partir de n'importe quelle plate-forme de dév (GitHub, Bitbucket, GitLab ...)
16. Comment faire pour cloner une *branche spécifique* d'un dépôt ? Par exemple, le projet [nematus](https://github.com/EdinburghNLP/nematus), en traduction neuronale, a une branche nommée *theano*. Comment cloner celle-ci, et pas les autres?
17. Que veut dire la commande `diff HEAD^ HEAD` ?
